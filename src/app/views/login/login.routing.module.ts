import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { RegisterFormComponent } from './../../components/forms/register-form/register-form.component';
import { LoginFormComponent } from './../../components/forms/login-form/login-form.component';
import { LoginComponent } from './login.component';

const userRoutes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children: [
      { path: 'login', component: LoginFormComponent },
      { path: 'register', component: RegisterFormComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule],
})
export class LoginRoutingModule {}
