import { UtilsModule } from './../utils/utils.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateEventComponent } from './create-event/create-event.component';
import { UpdateEventComponent } from './update-event/update-event.component';
import { DeleteEventComponent } from './delete-event/delete-event.component';
import { EventHeaderComponent } from './event-header/event-header.component';

@NgModule({
  declarations: [
    CreateEventComponent,
    UpdateEventComponent,
    DeleteEventComponent,
    EventHeaderComponent,
  ],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, UtilsModule],
})
export class EventModule {}
