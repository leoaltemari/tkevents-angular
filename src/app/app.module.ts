// Internal
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// Services
import { AuthGuard } from './guards/auth-guard.guard';
import { EventsService } from './services/events/events.service';
import { UserService } from './services/user/user.service';

// Modules
import { FormsModule } from '@angular/forms';
import { NavBarModule } from './components/nav-bar/nav-bar.module';
import { AppRoutingModule } from './app-routing.module';

// Components
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    NavBarModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
  ],
  providers: [UserService, EventsService, AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
