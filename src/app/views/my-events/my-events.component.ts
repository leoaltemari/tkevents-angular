import { EventsService } from './../../services/events/events.service';
import { Events } from './../../models/Event';
import { Component, OnInit } from '@angular/core';

import { take } from 'rxjs/operators';

@Component({
  selector: 'app-my-events',
  templateUrl: './my-events.component.html',
  styleUrls: ['./my-events.component.scss'],
})
export class MyEventsComponent implements OnInit {
  userEvents: Events[];

  constructor(private eventService: EventsService) {}

  ngOnInit(): void {
    // Get required user data
    const user = JSON.parse(window.localStorage.getItem('userData'));
    const id = user.data._id;
    const token = user.token;

    try {
      this.eventService
        .getUserEvents(id, token)
        .pipe(take(1))
        .subscribe((events) => {
          window.localStorage.setItem('userEvents', JSON.stringify(events));
          this.userEvents = events;
        });
    } catch (err) {
      console.log(err);
    }
  }
}
