import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';

// Services
import { EventsService } from './../../../services/events/events.service';

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss'],
})
export class CreateEventComponent implements OnInit {
  // Internal
  forms: FormGroup;
  errors: string[];
  successMsg: string;

  // User data required
  userId: string;
  userToken: string;

  constructor(
    private formBuilder: FormBuilder,
    private eventsService: EventsService
  ) {}

  ngOnInit(): void {
    this.forms = this.formBuilder.group({
      name: [
        null,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(20),
        ],
      ],
      description: [null, Validators.required],
      startDate: [null, Validators.required],
      startHour: [
        null,
        [Validators.required, Validators.max(23), Validators.min(0)],
      ],
      finishDate: [null, Validators.required],
      finishHour: [
        null,
        [Validators.required, Validators.max(23), Validators.min(0)],
      ],
    });

    // Get required user data from local storage
    const userData = JSON.parse(window.localStorage.getItem('userData'));
    this.userId = userData.data._id;
    this.userToken = userData.token;
  }

  validField(field): boolean {
    return !field.valid && field.touched;
  }

  onSubmit(): void {
    // Reset
    this.errors = [];
    this.successMsg = '';

    try {
      this.eventsService
        .create(this.forms.value, this.userId, this.userToken)
        .pipe(take(1))
        .subscribe((res) => {
          if (res.ok) {
            this.successMsg = res.message;
          } else {
            if (res.errors.length > 0) {
              this.errors = res.errors;
            } else {
              this.errors.push(res.message);
            }
          }
        });
    } catch (err) {
      console.log(err);
    }
  }
}
