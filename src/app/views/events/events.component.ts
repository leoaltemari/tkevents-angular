import { EventsService } from './../../services/events/events.service';
import { Component, OnInit } from '@angular/core';

import { Events } from './../../models/Event';

import { take } from 'rxjs/operators';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss'],
})
export class EventsComponent implements OnInit {
  selectedDate: string;
  events: Events[];

  constructor(private eventService: EventsService) {}

  ngOnInit(): void {
    try {
      this.eventService
        .getEvents()
        .pipe(take(1))
        .subscribe((res) => {
          this.events = res;
        });
    } catch (err) {
      console.log(err);
    }
  }

  getEventsByDate(): void {
    try {
      this.eventService
        .getEventsByDate(this.selectedDate)
        .pipe(take(1))
        .subscribe((events) => (this.events = events));
    } catch (err) {
      console.log(err);
    }
  }
}
