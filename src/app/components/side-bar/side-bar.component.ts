import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.scss'],
})
export class SideBarComponent implements OnInit {
  showSideBar = true;
  constructor() {}

  ngOnInit(): void {}

  changeSideBar(): void {
    this.showSideBar = !this.showSideBar;
  }
}
