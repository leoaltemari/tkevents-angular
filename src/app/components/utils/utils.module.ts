import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputErrorComponent } from './input-error/input-error.component';

// Component
import { ErrorComponent } from './error/error.component';

@NgModule({
  declarations: [InputErrorComponent, ErrorComponent],
  imports: [CommonModule, FormsModule],
  exports: [InputErrorComponent, ErrorComponent],
})
export class UtilsModule {}
