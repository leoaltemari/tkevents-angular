import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

// Components
import { NavBarComponent } from './nav-bar.component';
import { LinksComponent } from './links/links.component';
import { ToggleComponent } from './toggle/toggle.component';

@NgModule({
  declarations: [LinksComponent, ToggleComponent, NavBarComponent],
  imports: [CommonModule, FormsModule, RouterModule],
  exports: [NavBarComponent],
})
export class NavBarModule {}
