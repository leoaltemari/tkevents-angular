import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';

// Models
import { Events } from './../../../models/Event';

// Services
import { EventsService } from './../../../services/events/events.service';

@Component({
  selector: 'app-update-event',
  templateUrl: './update-event.component.html',
  styleUrls: ['./update-event.component.scss'],
})
export class UpdateEventComponent implements OnInit {
  // Internal
  forms: FormGroup;
  errors: string[];
  successMsg: string;
  eventSelected = 'Selecione um evento';
  eventSelectedData = new Events();

  // User data required
  userToken: string;
  userEvents: Events[];
  eventId: string;

  constructor(
    private formBuilder: FormBuilder,
    private eventsService: EventsService
  ) {}

  ngOnInit(): void {
    this.forms = this.formBuilder.group({
      name: [
        null,
        [
          Validators.required,
          Validators.minLength(5),
          Validators.maxLength(20),
        ],
      ],
      description: [null, Validators.required],
      startDate: [null, Validators.required],
      startHour: [
        null,
        [Validators.required, Validators.max(23), Validators.min(0)],
      ],
      finishDate: [null, Validators.required],
      finishHour: [
        null,
        [Validators.required, Validators.max(23), Validators.min(0)],
      ],
    });

    // Get required user data
    const userData = JSON.parse(window.localStorage.getItem('userData'));
    this.userToken = userData.token;

    // Get user events
    setTimeout(
      () =>
        (this.userEvents = JSON.parse(
          window.localStorage.getItem('userEvents')
        )),
      1000
    );
  }

  validField(field): boolean {
    return !field.valid && field.touched;
  }

  onSubmit(): void {
    // Reset
    this.errors = [];
    this.successMsg = '';

    try {
      this.eventsService
        .update(this.forms.value, this.eventId, this.userToken)
        .pipe(take(1))
        .subscribe((res) => {
          if (res.ok) {
            this.successMsg = res.message;
          } else {
            if (res.errors.length > 0) {
              this.errors = res.errors;
            } else {
              this.errors.push(res.message);
            }
          }
        });
    } catch (err) {
      console.log(err);
    }
  }

  findEventData(): void {
    this.eventSelectedData = this.userEvents.find((event) => {
      return event.name === this.eventSelected;
    });

    this.eventId = this.eventSelectedData._id;

    this.forms.setValue({
      name: this.eventSelectedData.name,
      description: this.eventSelectedData.description,
      startDate: this.eventSelectedData.startDate.toString().slice(0, 10),
      startHour: this.eventSelectedData.startHour,
      finishDate: this.eventSelectedData.finishDate.toString().slice(0, 10),
      finishHour: this.eventSelectedData.finishHour,
    });
  }
}
