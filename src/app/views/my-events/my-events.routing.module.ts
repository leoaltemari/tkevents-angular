import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { DeleteEventComponent } from './../../components/event/delete-event/delete-event.component';
import { UpdateEventComponent } from './../../components/event/update-event/update-event.component';
import { CreateEventComponent } from './../../components/event/create-event/create-event.component';
import { MyEventsComponent } from './my-events.component';

const userRoutes: Routes = [
  {
    path: '',
    component: MyEventsComponent,
    children: [
      { path: 'create', component: CreateEventComponent },
      { path: 'update', component: UpdateEventComponent },
      { path: 'delete', component: DeleteEventComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(userRoutes)],
  exports: [RouterModule],
})
export class MyEventsRoutingModule {}
