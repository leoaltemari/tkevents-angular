import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { UserService } from './../../../services/user/user.service';

@Component({
  selector: 'app-toggle',
  templateUrl: './toggle.component.html',
  styleUrls: ['./toggle.component.scss'],
})
export class ToggleComponent implements OnInit {
  // Internal
  checked: boolean;
  @Input() userName: string;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit(): void {}

  closeMenu(): void {
    this.checked = !this.checked;
  }

  logOutUser(): void {
    this.userName = null;
    this.userService.changeUser(null);
    this.router.navigate(['/home']);
    this.closeMenu();
  }
}
