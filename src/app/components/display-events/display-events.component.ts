import { Component, OnInit, OnChanges, Input, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

// Model
import { Events } from './../../models/Event';

@Component({
  selector: 'app-display-events',
  templateUrl: './display-events.component.html',
  styleUrls: ['./display-events.component.scss'],
})
export class DisplayEventsComponent implements OnInit, OnChanges {
  // Internal
  @Input() eventsInput: Events[];
  events: any[];
  eventsImages = [
    // Random images
    'evento1.jpg',
    'evento2.jpg',
    'evento3.jpg',
    'evento4.jpg',
    'evento5.jpg',
  ];

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.events = this.eventsInput;
  }

  // Select one image from the image array randomly
  randomImage(): string {
    const rand = ((Math.random() * 10) % this.eventsImages.length) + '';

    const position = parseInt(rand, 10);

    return `../../../assets/images/${this.eventsImages[position]}`;
  }

  // Get the event that is being hovered and sets its paragraph to be shown
  hoverEvent(id): void {
    this.events.forEach((event) => {
      if (event && event._id === id) {
        event.showParagraph = !event.showParagraph;
      }
    });
  }

  // Every time the 'eventsInput' changes
  ngOnChanges(): void {
    // Change the variable that contains the events to not modify the events variable
    setTimeout(() => {
      this.events = this.eventsInput;
    }, 200);

    // Set a image to the event
    this.events = this.eventsInput?.map((event) => {
      event.img = this.randomImage();
    });

    this.events?.forEach((event) => {
      if (event) {
        event.showParagraph = false;
      }
    });
  }
}
