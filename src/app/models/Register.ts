'use strict';

export interface Register {
  name: string;
  email: string;
  password: string;
}

export interface RegisterRequest {
  ok: boolean;
  message: string;
}
