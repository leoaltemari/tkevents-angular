import { MyEventsRoutingModule } from './my-events.routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { DisplayEventsModule } from './../../components/display-events/display-events.module';
import { EventModule } from './../../components/event/event.module';
import { SideBarComponent } from './../../components/side-bar/side-bar.component';
import { MyEventsComponent } from './my-events.component';

@NgModule({
  declarations: [MyEventsComponent, SideBarComponent],
  imports: [
    CommonModule,
    EventModule,
    DisplayEventsModule,
    MyEventsRoutingModule,
  ],
})
export class MyEventsModule {}
