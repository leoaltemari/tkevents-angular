import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Subscription } from 'rxjs';
import { filter } from 'rxjs/operators';

// Services
import { UserService } from './../../services/user/user.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss'],
})
export class NavBarComponent implements OnInit, OnDestroy {
  // Internal
  userName: string;
  userEmail: string;
  sub: Subscription[] = []; // Contains all subscriptions done in this component

  // Control navbar at login page(nav bar doestn shows at the login/register page)
  isAtloginPage = false;
  currentRoute: string;

  constructor(private router: Router, private userService: UserService) {}

  ngOnInit(): void {
    // Checks if the current page is the login page subscribing in the route events
    try {
      this.sub.push(
        this.router.events
          .pipe(filter((event: any) => event instanceof NavigationEnd))
          .subscribe((event) => {
            if (event.url.includes('login') || event.url.includes('register')) {
              this.isAtloginPage = true;
            } else {
              this.isAtloginPage = false;
            }
          })
      );
    } catch (err) {
      console.log(err);
    }

    // Try to retrieve the user data from the local storage
    const localStorageUser = JSON.parse(
      window.localStorage.getItem('userData')
    );
    if (localStorageUser) {
      this.userService.changeUser(localStorageUser);
    }

    // Subscribe in the user data at user service to get the user name and email for the navbar
    this.sub.push(
      this.userService.getUser().subscribe((uData) => {
        if (uData?.data) {
          this.userName = uData.data.name;
          this.userEmail = uData.data.email;

          // Removes extra characters from name and email to fit in the container
          if (this.userName.length > 17) {
            this.userName = this.userName.slice(0, 17);
            this.userName += '...';
          }

          if (this.userEmail.length > 21) {
            this.userEmail = this.userEmail.slice(0, 21);
            this.userEmail += '...';
          }
        }
      })
    );
  }

  ngOnDestroy(): void {
    // Unsubscribe all subscriptions done
    this.sub.forEach((subscription) => subscription.unsubscribe());
  }

  logOutUser(): void {
    this.userName = null;
    this.userEmail = null;
    this.userService.changeUser(null);
    this.router.navigate(['/home']);
  }
}
