'use strict';

import { Invitation } from './Invitation';

export interface User {
  _id: string;
  name: string;
  email: string;
  invitations: Invitation[];
  roles: string[];
}

export class UserRequest {
  data: User;
  token: string;
  ok: boolean;
  errors: string[];
}
