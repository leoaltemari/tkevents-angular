import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

// Models
import { Events, Event, EventRequest } from './../../models/Event';

@Injectable({
  providedIn: 'root',
})
export class EventsService {
  // private readonly url = 'http://3.235.28.74:3333/events';
  private readonly url = 'http://localhost:8081/events';

  constructor(private http: HttpClient) {}

  // GET
  getEvents(): Observable<Events[]> {
    return this.http.get<Events[]>(this.url);
  }

  getEventsByDate(date: string): Observable<Events[]> {
    return this.http.get<Events[]>(`${this.url}/${date}`);
  }

  getEventById(id: string): Observable<Events> {
    return this.http.get<Events>(`${this.url}/id/${id}`);
  }

  getUserEvents(userId: string, token: string): Observable<Events[]> {
    return this.http.get<Events[]>(`${this.url}/user/${userId}/${token}`);
  }

  // POST
  create(
    eventData: Event,
    userId: string,
    userToken: string
  ): Observable<EventRequest> {
    const data = {
      ...eventData,
      token: userToken,
      creator: userId,
    };

    return this.http.post<EventRequest>(`${this.url}`, data);
  }

  // PUT
  update(
    eventData: Event,
    eventId: string,
    userToken: string
  ): Observable<EventRequest> {
    const data = {
      ...eventData,
      token: userToken,
    };
    return this.http.put<EventRequest>(`${this.url}/${eventId}`, data);
  }

  // DELETE
  delete(eventId: string, userToken: string): Observable<EventRequest> {
    return this.http.delete<EventRequest>(
      `${this.url}/${userToken}/${eventId}`
    );
  }
}
