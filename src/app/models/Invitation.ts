'use strict';
import { User } from './User';
import { Events } from './Event';

export interface Invitation {
  whoInvited: User;
  event: Events;
  accepted: boolean;
}
