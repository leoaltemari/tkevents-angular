import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

// Services
import { UserService } from './../../../services/user/user.service';

// Models
import { RegisterRequest } from './../../../models/Register';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss'],
})
export class RegisterFormComponent implements OnInit {
  // internal
  errors: string[];
  registerSuccess = false;
  count = 5;

  // Forms
  forms: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // Form data
    this.forms = this.formBuilder.group({
      name: [null, [Validators.required, Validators.minLength(5)]],
      email: [null, [Validators.email, Validators.required]],
      password: [
        null,
        [
          Validators.required,
          Validators.pattern(
            '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$'
          ),
        ],
      ],
      confirmPassword: [null, [Validators.required]],
    });
  }

  onSubmit(): void {
    // Checks if the password are the same
    if (!this.comparePasswords()) {
      return;
    }

    // Removes the 'confirm password' from the form object
    const userRegisterData = {
      ...this.forms.value,
    };
    delete userRegisterData.confirmPassword;

    // Try to create a new user
    try {
      this.userService
        .createUser(userRegisterData)
        .pipe(take(1))
        .subscribe((res: RegisterRequest) => {
          this.errors = []; // reset errors list

          // If the user was created
          if (res.ok) {
            this.registerSuccess = true;

            // Countdown to redirect the user after 5 seconds to the login screen
            setInterval(() => {
              setTimeout(() => this.count--, 1000);
              if (this.count === 0) {
                this.router.navigate(['/user/login']);
              }
            }, 1000);
          } else {
            this.errors.push(res.message);
          }
        });
    } catch (err) {
      console.log(err);
    }
  }

  // Checks if the form field passed is valid
  validField(field): boolean {
    return !field.valid && field.touched;
  }

  comparePasswords(): any {
    const psw = this.forms?.value.password;
    const confirmPsw = this.forms?.value.confirmPassword;

    return confirmPsw && psw === confirmPsw;
  }
}
