import { UserComponent } from './user.component';
import { FormModule } from './../../components/forms/forms.module';
import { UserRoutingModule } from './user.routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [UserComponent],
  imports: [CommonModule, UserRoutingModule, FormModule],
})
export class UserModule {}
