import { FormsModule } from '@angular/forms';
import { EventsRoutingModule } from './events.routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Components
import { EventsComponent } from './events.component';
import { DisplayEventsModule } from './../../components/display-events/display-events.module';

@NgModule({
  declarations: [EventsComponent],
  imports: [
    CommonModule,
    DisplayEventsModule,
    FormsModule,
    EventsRoutingModule,
  ],
})
export class EventsModule {}
