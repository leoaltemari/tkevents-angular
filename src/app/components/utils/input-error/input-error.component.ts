import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-input-error',
  templateUrl: './input-error.component.html',
  styleUrls: ['./input-error.component.scss'],
})
export class InputErrorComponent implements OnInit {
  @Input() showOnly: boolean; // Condition to show an error or success
  @Input() hasError: boolean;
  @Input() errorMsg: string;
  @Input() successMsg: string;

  constructor() {}

  ngOnInit(): void {}
}
