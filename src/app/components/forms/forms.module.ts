import { RouterModule } from '@angular/router';
// Internal
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Modules
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UtilsModule } from './../utils/utils.module';

// Components
import { RegisterFormComponent } from './register-form/register-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { UpdateUserFormComponent } from './update-user-form/update-user-form.component';

@NgModule({
  declarations: [
    LoginFormComponent,
    RegisterFormComponent,
    UpdateUserFormComponent,
  ],
  imports: [
    CommonModule,
    UtilsModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
  ],
  exports: [LoginFormComponent, RegisterFormComponent, UpdateUserFormComponent],
})
export class FormModule {}
