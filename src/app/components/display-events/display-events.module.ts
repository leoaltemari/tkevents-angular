import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisplayEventsComponent } from './display-events.component';

@NgModule({
  declarations: [DisplayEventsComponent],
  imports: [CommonModule],
  exports: [DisplayEventsComponent],
})
export class DisplayEventsModule {}
