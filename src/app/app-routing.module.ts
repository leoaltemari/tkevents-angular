import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { ErrorComponent } from './components/utils/error/error.component';

// Guards
import { AuthGuard } from './guards/auth-guard.guard';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  {
    path: 'home',
    loadChildren: () =>
      import('./views/home/home.module').then((module) => module.HomeModule),
  },
  {
    path: 'events',
    loadChildren: () =>
      import('./views/events/events.module').then(
        (module) => module.EventsModule
      ),
  },
  {
    path: 'user',
    loadChildren: () =>
      import('./views/login/login.module').then((module) => module.LoginModule),
  },
  {
    path: 'myevents',
    loadChildren: () =>
      import('./views/my-events/my-events.module').then(
        (module) => module.MyEventsModule
      ),
    canActivate: [AuthGuard],
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('./views/user/user.module').then((module) => module.UserModule),
    canActivate: [AuthGuard],
  },
  {
    path: '**',
    component: ErrorComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
