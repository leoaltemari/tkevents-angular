import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';

// Models
import { UserRequest } from './../../../models/User';

// Service
import { UserService } from './../../../services/user/user.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  // Internal
  userRes: UserRequest;
  errors: string[];

  // Forms
  forms: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.forms = this.formBuilder.group({
      email: [null, [Validators.email, Validators.required]],
      password: [null, Validators.required],
    });
  }

  validField(field): boolean {
    return !field.valid && field.touched;
  }

  onSubmit(): void {
    // Reset
    this.errors = [];

    // Get forms values
    const email = this.forms.value.email;
    const password = this.forms.value.password;

    // Try to authenticate the user
    try {
      this.userService
        .authenticateUser(email, password)
        .pipe(take(1))
        .subscribe((res) => {
          if (res.ok) {
            this.userService.changeUser(res); // Sets the user data
            this.router.navigate(['/home']); // Redirects dot the homepage
          } else {
            this.errors = res.errors;
          }
        });
    } catch (err) {
      console.log(err);
    }
  }
}
