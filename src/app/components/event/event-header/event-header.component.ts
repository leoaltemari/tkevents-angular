import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-event-header',
  templateUrl: './event-header.component.html',
  styleUrls: ['./event-header.component.scss'],
})
export class EventHeaderComponent implements OnInit {
  // Internal
  @Input() title: string;
  @Input() image: string;
  constructor() {}

  ngOnInit(): void {
    this.image = `../../../../assets/icons/${this.image}_event_icon.png`;
  }
}
