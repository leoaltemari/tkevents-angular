import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';

// Models
import { User, UserRequest } from './../../models/User';
import { Register, RegisterRequest } from './../../models/Register';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private readonly url = 'http://localhost:8081/user';

  private user = new UserRequest();
  private userData$ = new BehaviorSubject(this.user);

  isAuthenticated: boolean;

  constructor(private http: HttpClient) {}

  // Makes a request to the API checking if this user exists
  authenticateUser(email: string, password: string): Observable<UserRequest> {
    const data = {
      email,
      password,
    };

    return this.http.post<UserRequest>(`${this.url}/login`, data);
  }

  // Returns the user data so that any componant can subscribe
  getUser(): Observable<UserRequest> {
    return this.userData$.asObservable();
  }

  userIsAuthenticated(): boolean {
    return this.isAuthenticated;
  }

  // Sets a new user
  changeUser(user: UserRequest): void {
    // If the argument passed is null, it removes the user from the local storage
    if (!user) {
      window.localStorage.removeItem('userData');
      window.localStorage.removeItem('userEvents');
      const nullUser = {
        data: null,
        token: null,
        errors: [],
        ok: false,
      };

      this.userData$.next(nullUser);
      this.isAuthenticated = false;
      return;
    }

    // Saves the user at the local storage and updates the userData to every component
    // that had subscribed on it
    const userData = {
      data: user.data,
      token: user.token,
    };

    localStorage.setItem('userData', JSON.stringify(userData));
    this.userData$.next(user);
    this.isAuthenticated = true;
  }

  // POST
  createUser(data: Register): Observable<RegisterRequest> {
    return this.http.post<RegisterRequest>(`${this.url}`, data);
  }

  // PUT
  updateUser(data: User, id: string): Observable<RegisterRequest> {
    return this.http.put<RegisterRequest>(`${this.url}/${id}`, data);
  }
}
