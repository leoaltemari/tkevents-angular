import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { take } from 'rxjs/operators';

// Models
import { Events } from './../../../models/Event';

// Services
import { EventsService } from './../../../services/events/events.service';

@Component({
  selector: 'app-delete-event',
  templateUrl: './delete-event.component.html',
  styleUrls: ['./delete-event.component.scss'],
})
export class DeleteEventComponent implements OnInit {
  // Internal
  forms: FormGroup;
  successMsg: string;

  // User data required
  userToken: string;
  userEvents: Events[];
  eventId: string;

  constructor(
    private formBuilder: FormBuilder,
    private eventsService: EventsService
  ) {}

  ngOnInit(): void {
    this.forms = this.formBuilder.group({
      selectedEvent: ['Selecione um evento', [Validators.required]],
    });

    // Get required user data
    const userData = JSON.parse(window.localStorage.getItem('userData'));
    this.userToken = userData.token;

    // Get user events
    setTimeout(
      () =>
        (this.userEvents = JSON.parse(
          window.localStorage.getItem('userEvents')
        )),
      1000
    );
  }

  // Get the id of the event that is selected on the 'select' input
  getEventId(): string {
    const name = this.forms.value.selectedEvent;

    const eventData = this.userEvents.find((event) => {
      return event.name === name;
    });

    return eventData._id;
  }

  onSubmit(): void {
    // Reset
    this.successMsg = '';

    try {
      const id = this.getEventId();
      this.eventsService
        .delete(id, this.userToken)
        .pipe(take(1))
        .subscribe((res) => {
          if (res.ok) {
            this.successMsg = res.message;
          }
        });
    } catch (err) {
      console.log(err);
    }
  }
}
