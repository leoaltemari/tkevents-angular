'use strict';
import { User } from './User';

export class Events {
  // tslint:disable-next-line: variable-name
  _id: string;
  name: string;
  description: string;
  img: string;
  startDate: Date;
  finishDate: Date;
  startHour: number;
  finishHour: number;
  creator: User;
  active: boolean;
}

export interface Event {
  name: string;
  description: string;
  startDate: Date;
  finishDate: Date;
  startHour: number;
  finishHour: number;
  creator: User;
  active: boolean;
}

export interface EventRequest {
  ok: boolean;
  errors: string[];
  message: string;
}
