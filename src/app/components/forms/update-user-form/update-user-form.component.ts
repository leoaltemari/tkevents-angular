import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { take } from 'rxjs/operators';

// Services
import { UserService } from './../../../services/user/user.service';

// Models
import { User } from './../../../models/User';
import { RegisterRequest } from './../../../models/Register';

@Component({
  selector: 'app-update-user-form',
  templateUrl: './update-user-form.component.html',
  styleUrls: ['./update-user-form.component.scss'],
})
export class UpdateUserFormComponent implements OnInit, OnDestroy {
  // Internal
  userData: User;
  token: string;
  sub: Subscription;
  errors: string[];
  updateSuccess = false;

  // Forms
  forms: FormGroup;

  constructor(
    private userService: UserService,
    private formBuilder: FormBuilder
  ) {}

  ngOnInit(): void {
    // Get user data to display at the inputs
    this.sub = this.userService.getUser().subscribe((uData) => {
      this.userData = uData.data;
      this.token = uData.token;
    });

    // Initiate the form
    this.forms = this.formBuilder.group({
      name: [
        this.userData.name,
        [Validators.required, Validators.minLength(5)],
      ],
      email: [this.userData.email, [Validators.email, Validators.required]],
    });
  }

  ngOnDestroy(): void {
    // Unsubscribe all subscriptions
    this.sub.unsubscribe();
  }

  onSubmit(): void {
    // Reset
    this.resetErrors();

    // Removes the 'confirm password' from the form object
    const userRegisterData = {
      token: this.token,
      ...this.forms.value,
    };
    delete userRegisterData.confirmPassword;

    // Try to create a new user
    try {
      this.userService
        .updateUser(userRegisterData, this.userData._id)
        .pipe(take(1))
        .subscribe((res: RegisterRequest) => {
          // If the user was created
          if (res.ok) {
            this.updateSuccess = true;
            this.updateLocalUserData();
          } else {
            this.errors.push(res.message);
          }
        });
    } catch (err) {
      console.log(err);
    }
  }

  resetErrors(): void {
    this.errors = [];
    this.updateSuccess = false;
  }

  updateLocalUserData(): void {
    this.userData.email = this.forms.value.email;
    this.userData.name = this.forms.value.name;

    const updatedUserData = {
      data: this.userData,
      token: this.token,
      ok: true,
      errors: [],
    };

    this.userService.changeUser(updatedUserData);
  }

  // Checks if the form field passed is valid
  validField(field): boolean {
    return !field.valid && field.touched;
  }
}
